<?php
/**
 * Created by PhpStorm.
 * User: udingh
 * Date: 29-8-2017
 * Time: 9:47
 */
// header('Content-Type: application/json');
// use key 'http' even if you send the request to https://...

header('Content-Type: application/json');
// Setup cURL
$ch = curl_init('https://rg1.krypc.com:8080/WB/asset/balanceUser');
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded'
    ),
    CURLOPT_POSTFIELDS => $_POST
));

// Send the request
$response = curl_exec($ch);

// Check for errors
if($response === FALSE){
    die(curl_error($ch));
}
echo $response;
?>