<?php
/**
 * Created by PhpStorm.
 * User: udingh
 * Date: 29-8-2017
 * Time: 9:18
 */
header('Content-Type: application/json');
$data = json_decode(file_get_contents('php://input'), true);
// use key 'http' even if you send the request to https://...
$postData = $data;

// Setup cURL
$ch = curl_init('https://rg1.krypc.com:8080/WB/user/jsonSubmitStructure');
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => json_encode($postData)
));

// Send the request
$response = curl_exec($ch);

// Check for errors
if($response === FALSE){
    die(curl_error($ch));
}
echo $response;
?>