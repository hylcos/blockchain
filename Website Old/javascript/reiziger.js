$(function () {
	$('#myLayout').w2layout({
		name: 'myLayout',
		panels: [
			{ type: 'top', size: 60 },
			{ type: 'left', size: 150, resizable: true },
			{ type: 'right', size: 150, resizable: true },
			{ type: 'bottom', size: 60, resizable: true }
		]
	});
			
	// return current content
	w2ui['myLayout'].load('main', 'reizigerform.html', 'slide-left', function () {
		console.log('content loaded');
	});
	w2ui['myLayout'].load('bottom', 'footer.html', 'slide-left', function () {
		console.log('content loaded');
	});
	w2ui['myLayout'].load('top', 'header.html', 'slide-left', function () {
		console.log('content loaded');
	});
	w2ui.myLayout.show('top');
//});
//$(function (){
    $('#myForm').w2form({ 
        name   : 'myForm',
        fields : [
            { name: 'van', type: 'list', required: true },
            { name: 'naar',  type: 'list', required: true },
            { name: 'vertrek',   type: 'date', required: true}
        ],
        actions: {
            reset: function () {
                this.clear();
            },
            save: function () {
                this.save();
            }
        }
    });
//});
//$(function () {
	var cities = ['Amsterdam','Rotterdam','Den Haag', 'Gouda','Utrecht'];
	$('input[type=list]').w2field('list',{items:cities});
	$('input[type=list]').w2field('list',{items:cities});
	$('input[type=eu-date]').w2field('vertrek',  { format: 'dd.mm.yyyy' });
	w2ui.myLayout.show('top');
});
