import MaaS.Cities
#import MaaS

#import Dijkstra
import pprint

def main():
    pp = pprint.PrettyPrinter(indent=4)
    a = MaaS.CitiesGraph()
    G = a.G
    D = {}
    # for city in Cities.Cities:
    #     D[city] = Dijkstra.Graph.dijsktra(G, city, Modes.Train)
    # pp.pprint(D)
    G.render()
    pass


if __name__ == '__main__':
    main()
