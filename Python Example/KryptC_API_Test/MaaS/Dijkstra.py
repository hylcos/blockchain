from collections import defaultdict
import functools
import graphviz as gv
from .Modes import Modes


class Graph:
    def __init__(self):
        self.nodes_list = set()
        self.edges = defaultdict(list)
        self.distances = {}
        self.g1 = gv.Graph(format='png')

    def __len__(self):
        return len(self.nodes_list)

    def nodes(self):
        return self.nodes_list

    def add_node(self, value):
        self.nodes_list.add(value)
        self.g1.node(value)

    def add_edge(self, from_node, to_node, distance, mode):
        self.edges[from_node].append((to_node, mode))
        self.edges[to_node].append((from_node, mode))
        self.distances[(from_node, to_node)] = distance
        self.distances[(to_node, from_node)] = distance
        if all(x in mode for x in [Modes.Bus["name"], Modes.Taxi["name"], Modes.Train["name"]]):
            self.g1.edge(to_node, from_node, label=str(distance))
        elif all(x not in mode for x in [Modes.Train["name"]]):
            print(mode)
            self.g1.edge(to_node, from_node, label=str(distance), color="red")
        elif all(x not in mode for x in [Modes.Bus["name"]]):
            print(mode)
            self.g1.edge(to_node, from_node, label=str(distance), color="blue")
        elif all(x not in mode for x in [Modes.Bus["name"], Modes.Train["name"]]):
            self.g1.edge(to_node, from_node, label=str(distance), style="dashed")
        else:
            self.g1.edge(to_node, from_node, label=str(distance), color="green")

    def add_edge_one_side(self, from_node, to_node, distance, mode):
        self.edges[from_node].append((to_node, mode))
        self.distances[(from_node, to_node)] = distance
        if (to_node, from_node) not in self.distances:
            self.g1.edge(from_node, to_node, label=str(distance), style="dashed")

    def render(self):
        filename = self.g1.render(filename='img/g1')

    def dijsktra(graph, initial, mode):
        visited = {initial: 0}
        path = {}

        nodes = set(graph.nodes_list)

        while nodes:
            min_node = None
            for node in nodes:
                if node in visited:
                    if min_node is None:
                        min_node = node
                    elif visited[node] < visited[min_node]:
                        min_node = node

            if min_node is None:
                break

            nodes.remove(min_node)
            current_weight = visited[min_node]

            for edge in graph.edges[min_node]:
                weight = current_weight + graph.distances[(min_node, edge[0])]
                if (edge[0] not in visited or weight < visited[edge[0]]):
                    visited[edge[0]] = weight
                    path[edge[0]] = (min_node, edge[1])

        return visited, path
