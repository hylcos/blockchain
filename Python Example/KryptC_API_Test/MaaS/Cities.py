from .Modes import Modes
from .Dijkstra import *

AMSTERDAM = "Amsterdam"
GOUDA = "Gouda"
DEN_HAAG = "Den Haag"
UTRECHT = "Utrecht"
ROTTERDAM = "Rotterdam"
BREDA = "Breda"
DEN_BOSCH = "Den Bosch"
ALMERE = "Almere"
AMERSFOORT = "Amersfoort"
ZWOLLE = "Zwolle"
APELDOORN = "Apeldoorn"
ALLEEN_BUS_STAD = "AlleenBusStad"
ALLEEN_TREIN_STAD = "AlleenTreinStad"

# Cities = [
#     AMSTERDAM,
#     GOUDA,
#     UTRECHT,
#     DEN_HAAG,
#     ROTTERDAM
# ]


Cities = [
    
    GOUDA,
    UTRECHT,
    AMSTERDAM,
    DEN_HAAG,
    ROTTERDAM,
]

class CitiesGraph:
    def __init__(self):
        TRAIN_BUS_TAXI = [Modes.Bus["name"], Modes.Taxi["name"], Modes.Train["name"]]
        BUS_TAXI = [Modes.Bus["name"], Modes.Taxi["name"]]
        BUS_TRAIN = [Modes.Bus["name"], Modes.Train["name"]]
        TRAIN_TAXI = [Modes.Train["name"], Modes.Taxi["name"]]
        BUS = [Modes.Bus["name"]]
        TRAIN = [Modes.Train["name"]]
        TAXI = [Modes.Taxi["name"]]
        self.G = Graph()
        for city in Cities:
            self.G.add_node("%s" % city)

               #self.G.add_edge(AMSTERDAM, AMERSFOORT, 41, TRAIN_BUS_TAXI)
        #self.G.add_edge(AMSTERDAM, ALMERE, 23, TRAIN_BUS_TAXI)
        self.G.add_edge(AMSTERDAM, GOUDA, 35, BUS_TAXI)
        #self.G.add_edge(AMSTERDAM, ALLEEN_TREIN_STAD, 38, TRAIN)
        self.G.add_edge(DEN_HAAG, GOUDA, 30, TRAIN_BUS_TAXI)
        self.G.add_edge(DEN_HAAG, ROTTERDAM, 25, TRAIN_BUS_TAXI)
        self.G.add_edge(GOUDA, UTRECHT, 20, TRAIN_BUS_TAXI )
        self.G.add_edge(GOUDA, ROTTERDAM, 30, TRAIN_BUS_TAXI)
        self.G.add_edge(AMSTERDAM, UTRECHT, 40, TRAIN_BUS_TAXI)
        self.G.add_edge(AMSTERDAM, DEN_HAAG, 50, TRAIN_BUS_TAXI)

        #self.G.add_edge(BREDA, DEN_BOSCH, 40, TRAIN_BUS_TAXI)
        #self.G.add_edge(BREDA, ROTTERDAM, 45, TRAIN_BUS_TAXI)
        #self.G.add_edge(BREDA, UTRECHT, 59, BUS_TAXI)
        #self.G.add_edge(UTRECHT, DEN_BOSCH, 44, TRAIN_BUS_TAXI)
        #self.G.add_edge(UTRECHT, APELDOORN, 60, TRAIN_BUS_TAXI)
        #self.G.add_edge(UTRECHT, AMERSFOORT, 20, TRAIN_BUS_TAXI)
        #self.G.add_edge(UTRECHT, ALMERE, 33, TRAIN_BUS_TAXI)
        #self.G.add_edge(UTRECHT, ALLEEN_BUS_STAD, 25, BUS)
        #self.G.add_edge(ZWOLLE, APELDOORN, 34, TRAIN_BUS_TAXI)
        #self.G.add_edge(ZWOLLE, ALMERE, 57, TRAIN_BUS_TAXI)
        #self.G.add_edge(ZWOLLE, AMERSFOORT, 60, TRAIN_BUS_TAXI)

        # self.G = Graph()
        # self.G.add_node("Amsterdam")
        # self.G.add_node("Gouda")
        # self.G.add_node("Den Haag")
        # self.G.add_node("Utrecht")
        # self.G.add_node("Rotterdam")
        #
        # self.G.add_edge("Amsterdam", "Utrecht", 40, TRAIN_BUS_TAXI)
        # self.G.add_edge("Amsterdam", "Den Haag", 50, TRAIN_BUS_TAXI)
        # self.G.add_edge("Amsterdam", "Gouda", 35, BUS_TAXI)
        # self.G.add_edge("Den Haag", "Gouda", 30, TRAIN_BUS_TAXI)
        # self.G.add_edge("Den Haag", "Rotterdam", 25, TRAIN_BUS_TAXI)
        # self.G.add_edge("Gouda", "Utrecht", 20, TRAIN_BUS_TAXI)
        # self.G.add_edge("Gouda", "Rotterdam", 30, TRAIN_BUS_TAXI)

        D = {}
        for city in Cities:
            D[city] = Graph.dijsktra(self.G, city, Modes.Taxi)
        #for k, v in D.items():
        #    print(k)
        #    for city, dis in v[0].items():
        #        if city != k:
        #            self.G.add_edge_one_side(k, city, dis, TAXI)
