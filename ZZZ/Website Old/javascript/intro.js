$(function () {
	$('#myLayout').w2layout({
		name: 'myLayout',
		panels: [
			{ type: 'top', size: 60 },
			{ type: 'left', size: 150, resizable: true },
			{ type: 'right', size: 150, resizable: true },
			{ type: 'bottom', size: 60, resizable: true }
		]
	});
			
	// return current content
	w2ui['myLayout'].load('main', 'introtext.html', 'slide-left', function () {
		console.log('content loaded');
	});
	w2ui['myLayout'].load('bottom', 'footer.html', 'slide-left', function () {
		console.log('content loaded');
	});
	w2ui['myLayout'].load('top', 'header.html', 'slide-left', function () {
		console.log('content loaded');
	});
	w2ui.myLayout.show('top');
});
	
