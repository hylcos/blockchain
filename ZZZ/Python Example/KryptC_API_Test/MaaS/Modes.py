class Modes:
    Bus = {
        "name": "Bus",
        "node": "https://rg1.krypc.com:8080/WB/blockTxn/getBlocksData",
        # "node": "https://rg1.krypc.com:8081/WB/blockTxn/getBlocksData",
        "freq": 2,
        "price": 0.03,
        "velocity": 40,
    }
    Taxi = {
        "name": "Taxi",
        "node": "https://rg1.krypc.com:8080/WB/blockTxn/getBlocksData",
        # "node": "https://rg1.krypc.com:8082/WB/blockTxn/getBlocksData",
        "freq": 60,
        "price": 0.5,
        "velocity": 80,
    }

    Train = {
        "name": "Train",
        "node": "https://rg1.krypc.com:8080/WB/blockTxn/getBlocksData",
        # "node": "https://rg1.krypc.com:8083/WB/blockTxn/getBlocksData",
        "freq": 4,
        "price": 0.1,
        "velocity": 60,
    }
