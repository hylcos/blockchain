from .Transporter import *
import requests
import json
import time


class Request:
    def __init__(self, requestid, start, end, timedate, client_name):
        self.requestid = requestid
        self.start = start
        self.end = end
        self.travelDateTime = timedate
        self.client_name = client_name


class MobilityProvider:
    def __init__(self, mode, name):
        self.transporter = Transporter(mode, name)
        self.url = mode["node"]
        self.highest = requests.post(self.url, data={"data": json.dumps({"index_From": "null"})}).json()["firstIndex"]
        print("Created mobility provider " + str(name["transporterID"]))

    def run(self):
        self.running = True
        while self.running:
            print(str(self.transporter.transporterID) + " looking for new requests")
            requests = self.filter_new_blocks(self.get_new_blocks())
            for request in requests:
                self.transporter.make_offer(request)
            time.sleep(10)

    def filter_new_blocks(self, blocks):
        requestIDs = list()
        for block in blocks:
            try:
                if "TransportRequest" in block["transactions"][0]["arguments"]:
                    jsonData = json.loads(block["transactions"][0]["arguments"][4])
                    requestIDs.append(Request(jsonData["requestID"], jsonData["pickupPoint"], jsonData["destination"],
                                              datetime.strptime(jsonData["travelDateTime"], "%Y-%m-%d %H:%M"),
                                              block["transactions"][0]["arguments"][1]))
            except IndexError:
                continue
        return requestIDs

    def get_new_blocks(self, index_from=None):
        index = "null" if not index_from else str(index_from)
        data = requests.post(self.url, data={"data": json.dumps({"index_From": index})}).json()
        new_blocks = list()
        if data["firstIndex"] == self.highest:
            return new_blocks
        if not data["lastIndex"] <= self.highest <= data["firstIndex"]:
            new_blocks.extend(data["resultObject"])
            new_blocks.extend(self.get_new_blocks(data["lastIndex"]))
            self.highest = new_blocks[0]["blockId"]
            return new_blocks
        else:
            for block in data["resultObject"]:
                if block["blockId"] > self.highest:
                    new_blocks.append(block)
            self.highest = new_blocks[0]["blockId"]
            return new_blocks

    def stop(self):
        self.running = False
