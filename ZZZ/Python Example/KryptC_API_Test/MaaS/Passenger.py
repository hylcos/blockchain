# -*- coding utf-8 -*-
# Hylco Uding - hylco.uding@intraffic.nl

"""
    KrypC Transporter API Wrapper
    --------
    Creates a wrapper style interface for communications with the KrypC API
"""

from datetime import datetime
from copy import copy
from .Cities import Cities
from requests import post
import requests
import json


class Passenger:
    KRYPC_URL = "https://rg1.krypc.com:8080/WB/user/jsonSubmitStructure"
    KRYPC_DATA_URL = "https://rg1.krypc.com:8080/WB/asset/balanceUser"

    TransportRequest = {"senderEmail": "", "receiverEmail": "operator2", "userType": "user",
                        "senderpassword": "password", "programName": "INTRAFFIC", "messageId": "TransportRequest",
                        "jsonData": {"pickupPoint": "", "filerCirteria": "Cost",
                                     "travelDateTime": "", "requestID": "", "destination": "",
                                     "customerID": "CUST3", "staticID": 1}}

    TravelOptions = {"senderEmail": "", "receiverEmail": "", "userType": "user", "senderpassword": "password",
                     "programName": "INTRAFFIC", "messageId": "TravelOptions", "jsonData": {"requestID": ""}}

    QuoteSelection = {"senderEmail": "", "receiverEmail": "", "userType": "user", "senderpassword": "password",
                      "programName": "INTRAFFIC", "messageId": "QuoteSelection",
                      "jsonData": {"transporterID": "", "amount": {"qty": 0, "assetName": "transportcoin"},
                                   "requestID": "", "serviceTime": ""}}
    AssetBalance = {"userMail": "", "programName": "INTRAFFIC"}
    Passenger = ['mikell', 'andria', 'michell']

    def __init__(self, a="mikell"):
        """
        Creates a Passenger object. This function assumes the passenger name is mikell and transport username is stables
        :param a: String, name of Passenger
        """
        self.user = a
        self._user = "stables"
        self.offers = dict()

    def make_request(self, request, _from, _to, time=datetime.now().strftime("%Y-%m-%d %H:%M")):
        """
        Creates a new request
        TODO: have KrypC create an AI requestID
        :param request: the request id
        :param _from: name of the place being traveled from
        :param _to: name of the place being traveled to
        :param time: time of being picked up
        :return: True if request has been made, False if not
        """
        if not (_from in Cities and _to in Cities):
            print("Unknown City")
            return False
        a = copy(self.TransportRequest)
        a["senderEmail"] = self.user
        a["jsonData"]["pickupPoint"] = _from
        a["jsonData"]["destination"] = _to
        a["jsonData"]["travelDateTime"] = time
        a["jsonData"]["requestID"] = str(request)

        r = post(self.KRYPC_URL, json=a)
        if r.json()["errorCode"] > 0:
            print(r.json()["errorDetails"])
            return False
        print(r.json()["uuid"])
        return True

    def load_offers(self, request):
        """
        Loads and formats the different bids made to this request.
        :param request: The request ID of the request
        :return: a formated list of offers
        """
        a = copy(self.TravelOptions)
        a["jsonData"]["requestID"] = str(request)
        a["receiverEmail"] = "stables"
        a["senderEmail"] = "mikell"
        r = post(self.KRYPC_URL, json=a)
        if r.json()["errorCode"] > 0:
            print(r.json()["errorMessage"])
            exit(-1)
        else:
            l = r.json()
            print(l)
            d = list()
            i = 0
            for _l in l["resultObject"]:
                data = _l["mapsByNameAndFieldValue"]
                print(data)
                _d = dict()
                _d["mode"] = data["mode"]["value"]
                _d["pickupPoint"] = data["pickupPoint"]["value"]
                _d["destination"] = data["destination"]["value"]
                _d["pickupTime"] = data["pickupTime"]["value"]
                _d["EstDropTime"] = data["EstDropTime"]["value"]
                _d["travelCost"] = data["travelCost"]["value"]["qty"]
                _d["transporterID"] = data["transporterID"]["value"]
                _d["travelTime"] = data["travelTime"]["value"]
                d.append(_d)
                print()
                print(i, data["mode"]["value"], data["travelCost"]["value"]["qty"], data["transporterID"]["value"])
                print(data["pickupTime"]["value"], data["EstDropTime"]["value"])
                print(data["pickupPoint"]["value"], data["destination"]["value"])
                i += 1
            return d

    def choose_offer(self, request, offers, number):
        """
        Chooses an request and post that to the KrypC API.
        :param request: The RequestID of the request that is being made
        :param offers: A list of offers, retrieved from load_offers
        :param number: The number of the bid being chosen. This number corresponds with the index in the offers object
        :return: True if bid is chosen correctly, False if not
        """
        c = offers[number]
        a = copy(self.QuoteSelection)
        a["senderEmail"] = "mikell"
        a["receiverEmail"] = "stables"
        a["jsonData"]["transporterID"] = c["transporterID"]
        a["jsonData"]["amount"]["qty"] = int(c["travelCost"])
        a["jsonData"]["requestID"] = str(request)
        a["jsonData"]["serviceTime"] = datetime.now().strftime("%d-%m-%Y %H:%M")
        print(a)
        r = post(self.KRYPC_URL, json=a)
        if r.json()["errorCode"] > 0:
            print(r.json()["errorMessage"])
            print(r.json()["errorDetails"])
            print(r.json())
            return False
        print(r.json())
        return True

    def get_balance(self):
        a = copy(self.AssetBalance)
        a["userMail"] = self.user
        r = post(self.KRYPC_DATA_URL, json=a)
        return r.json()["resultObject"][0]["humanWorldbalance"]
