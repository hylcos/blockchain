# -*- coding utf-8 -*-
# Hylco Uding - hylco.uding@intraffic.nl

"""
    KrypC Transporter API Wrapper
    --------
    Creates a wrapper style interface for communications with the KrypC API.
"""

import json
import math
from copy import copy
from datetime import datetime, timedelta
from random import randint

from requests import post

from .Cities import *
from .Dijkstra import *


class Transporter:
    """
        Transporter class which contains different functions for calling the KrypC API. Zorg ervoor dat men snapt wat
        het doel is van deze klasse binnen het MaaS BlockChain project.

        Attributes
        ----------

        KRYPC_URL:
            The url pointing to the KrypC API

        TransportOffers:
            Data needed for retrieving TransportOffers present in the BlockChain. An bid make to a Passenger has to \
            comply with one of the offers.

        TransportBid:
            Preset data for making an offer to a Passenger.

        viewBookingDetails:
            Preset data for retreiving booking data

        Transporters:
            A list of account eligible to be a Transporter

        ValidationCodes:
            A list of return values for validating
    """
    KRYPC_URL = "https://rg1.krypc.com:8080/WB/user/jsonSubmitStructure"

    TransportOffers = {"senderEmail": "operator1", "receiverEmail": "", "userType": "user",
                       "senderpassword": "password", "programName": "INTRAFFIC", "messageId": "TransportOffers",
                       "jsonData": {"requestID": ""}}

    TransportBid = {"senderEmail": "", "receiverEmail": "", "userType": "user", "senderpassword": "password",
                    "programName": "INTRAFFIC", "messageId": "TransporterBid",
                    "jsonData": {"mode": "", "pickupPoint": "", "transporterID": "",
                                 "pickupTime": "", "EstDropTime": "", "requestID": "",
                                 "destination": "", "travelCost": {"qty": 0, "assetName": "transportcoin"},
                                 "serviceTime": ""}}

    viewBookingDetails = {"senderEmail": "", "receiverEmail": "", "userType": "user",
                          "senderpassword": "password", "programName": "INTRAFFIC", "messageId": "viewBookingDetails",
                          "jsonData": {"requestID": ""}}

    Transporters = ['ekipade', 'stables', 'carpek']

    ValidationCodes = {"VALIDATED_FOR_ME": 0, "VALIDATED": 1, "NOT_VALIDATED": 2}

    def __init__(self, mode, name):
        """
        Creates a Transporter Object. Also sets the transporterID, name and money
        :param mode: Integer, transporterID of Transporter
        :param name: String, Screenname of Transporter.
        """
        self.transporterID = name["transporterID"]
        self.money = 0
        self.username = name["username"]
        self.url = mode["node"]
        self.freq = mode["freq"]
        self.price = mode["price"]
        self.velocity = mode["velocity"]
        self.mode = mode["name"]
        self.startmin = randint(0, 60) % (60 / self.freq)
        self.timetable = [self.startmin + (a * (60 / self.freq)) for a in range(self.freq)]
        print(self.timetable)
        graph = CitiesGraph().G
        self.shortest_path = {}
        for name in Cities:
            self.shortest_path[name] = Graph.dijsktra(graph, name, mode)

        print(self.shortest_path)
        print(self.startmin)

    def get_default_offers(self, user, request):
        """
        For making an offer the available offers should be retrieved. This is because of that an bid should comply with
         the offers available
        :param user: String, name of the user making a request. Should be one of the Transporters list.
        :param request: Integer, the requestID of the request made by the passengaer
        :return: A formated list of available options
        """
        a = copy(self.TransportOffers)
        a["receiverEmail"] = user
        a["jsonData"]["requestID"] = str(request)
        r = post(self.KRYPC_URL, json=a)
        if r.json()["errorCode"] > 0:
            print(r.json()["errorMessage"])
            exit(-1)
        else:
            l = r.json()
            d = list()

            for _l in l["resultObject"]:
                data = _l["mapsByNameAndFieldValue"]
                if data["transporterID"]["value"] == str(self.transporterID):
                    _d = dict()
                    _d["mode"] = data["mode"]["value"]
                    _d["pickupPoint"] = data["from"]["value"]
                    _d["destination"] = data["to"]["value"]
                    _d["requestedTime"] = data["requestedTime"]["value"]
                    _d["totalCost"] = data["totalCost"]["value"]
                    _d["totalTime"] = data["totalTime"]["value"]
                    _d["transporterID"] = data["transporterID"]["value"]
                    d.append(_d)
                    # print(data["transporterID"])
                    # print(data["mode"]["value"], data["totalCost"]["value"], data["totalTime"]["value"])
                    # print(data["from"]["value"], data["via"]["value"], data["to"]["value"])
            return d

    def make_offer(self, request):
        """
        Makes an offer to a request from an Passenger.
        -   This function first calls get_default_offers to get the default offers.
        -   Than selecting a random offer from that list.
        -   Calls calculatetime for calculating the start and end time
        -   Sends out the bid

        :param request: Integer, the requestID for the request.
        :return: True is made bid correctly, False if not.
        """
        # l = self.get_default_offers("operator2", request)
        # o = l[randint(0, len(l) - 1)]
        offer = copy(self.TransportBid)
        (a, d) = self.calculatetime(request.start, request.end, request.travelDateTime)
        _s = request.start
        _e = request.end
        route = list()
        # Calculate route

        _s = self.shortest_path[request.end][1][_s]
        while _s[0] != _e:
            route.append(_s)
            _s = self.shortest_path[request.end][1][_s[0]]
        route.append(_s)
        print(route)
        offer["jsonData"]["mode"] = self.mode
        offer["jsonData"]["pickupPoint"] = request.start
        offer["jsonData"]["transporterID"] = str(self.transporterID)
        offer["jsonData"]["pickupTime"] = a.strftime("%d-%m-%Y %H:%M")
        offer["jsonData"]["EstDropTime"] = d.strftime("%d-%m-%Y %H:%M")
        offer["jsonData"]["requestID"] = str(request.requestid)
        offer["jsonData"]["destination"] = request.end
        offer["jsonData"]["travelCost"]["qty"] = math.ceil(self.shortest_path[request.end][0][request.start] * self.price)
        offer["jsonData"]["via"] = route
        offer["jsonData"]["transportUsername"] = self.username
        offer["jsonData"]["serviceTime"] = datetime.now().strftime("%d-%m-%Y %H:%M")
        offer["senderEmail"] = self.username
        offer["receiverEmail"] = request.client_name
        json.dumps(offer)
        r = post(self.KRYPC_URL, json=offer, timeout=10)
        if r.json()["errorCode"] > 0:
            print(r.json()["errorMessage"])
            print(r.json()["errorDetails"])
            return False
        else:
            print(str(self.transporterID) + " made a bit on " + str(request))
            return True

    def calculatetime(self, start, end, startTime):
        """
        Calculates time based on the mode of travel, the requested time and the total time needed to travel.
        :param mode: String, mode of the transport
        :param requested_time: String, datetime in format "%Y-%m-%d %H:%M" (2017-08-26 2:50)
        :param total_time: Integer, total time of travel.
        :return: Tuple, with the first element being the departure time and the second element being the arrival time
        """
        traveltime = self.shortest_path[end][0][start] / self.velocity * 60
        d = list()
        for t in self.timetable:
            if startTime.minute > t:
                d.append(t + 60 - startTime.minute)
            else:
                d.append(t - startTime.minute)
        d.sort()
        minutes_to_next = d[0]
        a = startTime + timedelta(minutes=minutes_to_next)
        d = a + timedelta(minutes=traveltime)
        return (a, d)

    def validate_payment(self, request):
        """
        Checks is the request is payed for. And if there is payed for there is payed for this Transporter.
        :param request: Integer, the requestID for the request.
        :return:
            -   0 if the payment is validated and I'm the one being payed
            -   1 if the payment is validated but another transporter is being payed.
            -   2 if the payment is not yet validated.
        """
        a = copy(self.viewBookingDetails)
        a["senderEmail"] = "mikell"
        a["receiverEmail"] = "operator2"
        a["jsonData"]["requestID"] = str(request)
        r = post(self.KRYPC_URL, json=a)
        try:
            details = json.loads(r.json()["errorDetails"])
            if details["transporterID"] == str(self.transporterID):
                self.money = details["serviceCost"]
                print(self.name + " heeft een bod gewonnen")
                return self.ValidationCodes["VALIDATED_FOR_ME"]
            else:
                return self.ValidationCodes["VALIDATED"]

        except json.decoder.JSONDecodeError as e:
            return self.ValidationCodes["NOT_VALIDATED"]
