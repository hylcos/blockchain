from MaaS import *
import threading
import MaaS


def main():
    m1 = MobilityProvider(Modes.Taxi,  Users.stables)
    m2 = MobilityProvider(Modes.Train, Users.ekipade)
    m3 = MobilityProvider(Modes.Bus, Users.carpek)
    _m = Modes.Train
    _m["freq"] = 10
    _m["price"] = 0.15
    m4 = MobilityProvider(_m, Users.carpek)

    t1 = threading.Thread(target=m1.run)
    t2 = threading.Thread(target=m2.run)
    t3 = threading.Thread(target=m3.run)
    # t4 = threading.Thread(target=m4.run)

    t1.start()
    t2.start()
    t3.start()
    # t4.start()
    pass


if __name__ == "__main__":
    main()
