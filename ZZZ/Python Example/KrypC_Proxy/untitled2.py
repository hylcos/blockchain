from flask import Flask, request, jsonify
import requests
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)


@app.route('/a', methods=["POST"])
def json_post():
    print("Nu wordt een post gedaan")
    r = requests.post("https://rg1.krypc.com:8080/WB/user/jsonSubmitStructure", json=request.json)
    print(r.text)
    return jsonify(r.json())


@app.route('/b', methods=["POST"])
def form_post():
    return jsonify(requests.post("https://rg1.krypc.com:8080/WB/asset/balanceUser", data=request.form).json())


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5003)
