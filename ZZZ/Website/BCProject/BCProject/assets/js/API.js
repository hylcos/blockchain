var cities = [
    "Amsterdam",
    "Gouda",
    "Utrecht",
    "Den Haag",
    "Rotterdam",
    "Breda",
    "Den Bosch",
    "Almere",
    "Amersfoort",
    "Zwolle",
    "Apeldoorn",
    "AlleenBusStad"
];
var requestID = 88;
var KRYPC_USER_URL = "https://rg1.krypc.com:8080/WB/user/jsonSubmitStructure";
// var KRYPC_USER_URL = "KrypCProxy/jsonSubmitStructure.php";
// var KRYPC_DATA_URL = "KrypCProxy/userBalance.php";
var KRYPC_DATA_URL = "https://rg1.krypc.com:8080/WB/asset/balanceUser";
// var KRYPC_USER_URL = "http://localhost:5003/a";
// var KRYPC_DATA_URL = "http://localhost:5003/b";
var modes = {'Bus': [5, 30], 'Taxi': [2, 6], 'Train': [8, 20]}

function Passenger(username, password) {
    this.user = username;
    this.pass = password;
    this.offers = {};
    this.requestID = requestID;
    //p = pickupoint
    //d = destination
    //t = time
    this.login = function () {

        var deferred = new $.Deferred();
        a = {
            "senderEmail": this.user,
            "receiverEmail": "operator2",
            "userType": "user",
            "senderpassword": this.pass,
            "programName": "INTRAFFIC",
            "messageId": "TransportRequest",
            "jsonData": {
                "pickupPoint": "Utrecht",
                "filerCirteria": "Cost",
                "travelDateTime": formatDate2(new Date()),
                "requestID": "1",
                "destination": "Rotterdam",
                "customerID": "CUST3",
                "staticID": 1
            }
        };
        $.support.cors = true;
        console.log(a);
        $.ajax({
            url: KRYPC_USER_URL,
            type: 'POST',
            data: JSON.stringify(a),
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            success: function (data) {
                console.log(data);
                deferred.resolve(data);
            },
            error: function (data) {
                console.info(data);
                deferred.resolve(data);
            }
        });
        return deferred.promise();
    };
    this.makeRequest = function (p, d, t) {

        var deferred = new $.Deferred();
        if (cities.indexOf(p) == -1 || cities.indexOf(d) == -1) {
            console.log("Unknown Cities");
        }
        a = {
            "senderEmail": this.user,
            "receiverEmail": "operator2",
            "userType": "user",
            "senderpassword": this.pass,
            "programName": "INTRAFFIC",
            "messageId": "TransportRequest",
            "jsonData": {
                "pickupPoint": p,
                "filerCirteria": "Cost",
                "travelDateTime": t,
                "requestID": String(requestID),
                "destination": d,
                "customerID": "CUST3",
                "staticID": 1
            }
        };
        console.log(a);
        $.ajax({
            url: KRYPC_USER_URL,
            type: 'POST',
            data: JSON.stringify(a),
            dataType: 'json',
            contentType: 'application/json',
            async: true,
            success: function (data) {
                console.info(data);
                deferred.resolve(data);
            },
            error: function (data, e) {
                console.info(data);
                console.info(e);
                if (e == "timeout") {
                    deferred.resolve({"errorCode": 1, "errorDetails": "Timeout met KrypC API"})
                }
                if (e == "error") {
                    deferred.resolve({"errorCode": 1, "errorDetails": "Kan niet verbinden met KrypC API"})
                }
                deferred.resolve(data);
            },
            timeout: 20000
        });
        return deferred.promise();
    };
    this.loadOffers = function () {
        var deferred = new $.Deferred();
        a = {
            "senderEmail": this.user,
            "receiverEmail": "stables",
            "userType": "user",
            "senderpassword": this.pass,
            "programName": "INTRAFFIC",
            "messageId": "TravelOptions",
            "jsonData": {
                "requestID": String(requestID)
            }
        };
        d = [];
        $.ajax({
            url: KRYPC_USER_URL,
            type: 'POST',
            data: JSON.stringify(a),
            crossDomain: true,
            dataType: 'json',
            contentType: 'application/json',
            async: true,
            success: function (data) {
                console.log(a);
                console.info(data);
                if (data["errorCode"] == 0) {
                    var myArray = data["resultObject"];
                    var arrayLength = myArray.length;
                    for (var i = 0; i < arrayLength; i++) {
                        var offer = myArray[i]["mapsByNameAndFieldValue"];
                        _d = {};
                        _d["mode"] = offer["mode"]["value"]
                        _d["pickupPoint"] = offer["pickupPoint"]["value"]
                        _d["destination"] = offer["destination"]["value"]
                        _d["pickupTime"] = offer["pickupTime"]["value"]
                        _d["EstDropTime"] = offer["EstDropTime"]["value"]
                        _d["travelCost"] = offer["travelCost"]["value"]["qty"]
                        _d["transporterID"] = offer["transporterID"]["value"]
                        _d["travelTime"] = offer["travelTime"]["value"]
                        _d["transportUsername"] = myArray[i]["sender"]
                        d.push(_d);
                    }
                } else {
                    deferred.resolve({"errorCode": data["errorCode"], "errorDetails": data["errorDetails"]});
                    return deferred.promise();
                }
                deferred.resolve(d);
            },
            timeout: 10000,
            error: function (xhr, e) {
                if (e == "timeout") {
                    deferred.resolve({"errorCode": 1, "errorDetails": "Kan niet verbinden met KrypC API"})
                }
                deferred.resolve(data);
            }
        });

        return deferred.promise();
    };
    this.chooseOffer = function (t, c, r) {
        var deferred = new $.Deferred();
        a = {
            "senderEmail": this.user,
            "receiverEmail": r,
            "userType": "user",
            "senderpassword": this.pass,
            "programName": "INTRAFFIC",
            "messageId": "QuoteSelection",
            "jsonData": {
                "transporterID": t,
                "amount": {
                    "qty": parseInt(c),
                    "assetName": "transportcoin"
                },
                "requestID": String(requestID),
                "serviceTime": formatDate(new Date())
            }
        };
        console.log(a);
        $.support.cors = true;
        $.ajax({
            url: KRYPC_USER_URL,
            type: 'POST',
            data: JSON.stringify(a),
            dataType: 'json',
            contentType: 'application/json',
            async: true,
            success: function (data) {
                console.log(data);
                deferred.resolve(data);
            },
            error: function (data) {
                console.info(data);
                deferred.resolve(data);
            }
        });

        return deferred.promise()
    };
    this.getBooking = function () {
        var deferred = new $.Deferred();
        a = {
            "senderEmail": this.user,
            "receiverEmail": "operator2",
            "userType": "user",
            "senderpassword": this.pass,
            "programName": "INTRAFFIC",
            "messageId": "viewBookingDetails",
            "jsonData": {"requestID": String(requestID)}
        };
        $.ajax({
            url: KRYPC_USER_URL,
            type: 'POST',
            data: JSON.stringify(a),
            dataType: 'json',
            contentType: 'application/json',
            async: true,
            success: function (data) {
                console.log(data);
                deferred.resolve(data);
            },
            error: function (data) {
                console.info(data);
                deferred.resolve(data);
            }
        });
        return deferred.promise();
    };
    this.getHistory = function (s, e) {
        var deferred = new $.Deferred();
        a = {
            "senderEmail": this.user,
            "receiverEmail": "operator1",
            "userType": "user",
            "senderpassword": this.pass,
            "programName": "INTRAFFIC",
            "messageId": "TransactonReport",
            "jsonData": {
                "fromDate": formatDate3(s),
                "toDate": formatDate3(e)
            }
        };
        $.ajax({
            url: KRYPC_USER_URL,
            type: 'POST',
            data: JSON.stringify(a),
            dataType: 'json',
            contentType: 'application/json',
            async: true,
            success: function (data) {

                console.log(data);
                if (data["errorCode"] == 0) {
                    d = [];
                    var myArray = data["resultObject"];
                    var arrayLength = myArray.length;
                    for (var i = 0; i < arrayLength; i++) {
                        var mapsByNameAndFieldValue = myArray[i]["mapsByNameAndFieldValue"];
                        console.log(mapsByNameAndFieldValue);
                        _d = {};
                        Object.keys(mapsByNameAndFieldValue).forEach(function (currentKey) {
                            _d[currentKey] = mapsByNameAndFieldValue[currentKey]["value"];
                        });
                        d.push(_d);
                    }
                    console.log(d);
                    deferred.resolve(d);

                } else {
                    deferred.resolve({"errorCode": data["errorCode"], "errorDetails": data["errorDetails"]});
                    return deferred.promise();
                }
            },
            timeout: 10000,
            error: function (xhr, e) {
                if (e == "timeout") {
                    deferred.resolve({"errorCode": 1, "errorDetails": "Kan niet verbinden met KrypC API"})
                }
                deferred.resolve(data);
            }
        });
        return deferred.promise();
    };
}

function Transporter(a, b) {
    this.transporterID = a;
    this.name = b;
    this.money = 0;
    this.getOffers = function () {
        a = {
            "senderEmail": "operator1",
            "receiverEmail": "operator2",
            "userType": "user",
            "senderpassword": "password",
            "programName": "INTRAFFIC",
            "messageId": "TransportOffers",
            "jsonData": {
                "requestID": String(requestID)
            }
        };
        d = [];
        $.ajax({
            url: KRYPC_USER_URL,
            type: 'POST',
            data: JSON.stringify(a),
            dataType: 'json',
            context: this,
            async: false,
            success: function (data) {
                console.info(data);
                var myArray = data["resultObject"];
                var arrayLength = myArray.length;
                for (var i = 0; i < arrayLength; i++) {
                    var offer = myArray[i]["mapsByNameAndFieldValue"];
                    if (offer["transporterID"]["value"] == String(this.transporterID)) {
                        _d = {};
                        _d["mode"] = offer["mode"]["value"];
                        _d["pickupPoint"] = offer["from"]["value"];
                        _d["destination"] = offer["to"]["value"];
                        _d["requestedTime"] = offer["requestedTime"]["value"];
                        _d["totalCost"] = offer["totalCost"]["value"];
                        _d["totalTime"] = offer["totalTime"]["value"];
                        _d["transporterID"] = offer["transporterID"]["value"];
                        d.push(_d);
                    }
                }

            }
        });
        return d;
    };
    this.makeOffer = function () {
        offers = this.getOffers();
        o = offers[getRandomInt(0, offers.length - 1)]
        var d = new Date(Date.parse(o["requestedTime"]));
        s = getRandomInt(modes[o["mode"]][0], modes[o["mode"]][1]);
        a = {
            "senderEmail": "stables",
            "receiverEmail": "mikell",
            "userType": "user",
            "senderpassword": "password",
            "programName": "INTRAFFIC",
            "messageId": "TransporterBid",
            "jsonData": {
                "mode": o["mode"],
                "pickupPoint": o["pickupPoint"],
                "transporterID": o["transporterID"],
                "pickupTime": formatDate(new Date(d.getTime() + s * 60000)),
                "EstDropTime": formatDate(new Date(d.getTime() + (s + o["totalTime"]) * 60000)),
                "requestID": String(requestID),
                "destination": o["destination"],
                "travelCost": {"qty": o["totalCost"], "assetName": "transportcoin"},
                "serviceTime": formatDate(new Date())
            }
        };
        console.log(a);
        $.ajax({
            url: KRYPC_USER_URL,
            type: 'POST',
            data: JSON.stringify(a),
            dataType: 'json',
            async: false,
            success: function (data) {
                console.info(data);
            }
        });
    };
}

function getBalance(user) {
    var deferred = new $.Deferred();
    a = {"userMail":user,"programName":"INTRAFFIC"};
    $.support.cors = true;
    console.log(a);
    $.ajax({
        url: KRYPC_DATA_URL,
        type: 'POST',
        data: JSON.stringify(a),
        dataType: 'json',
        contentType: 'application/json',
        async: true,
        success: function (data) {
            console.log(data);
            deferred.resolve(data);
        },
        error: function (data) {
            console.info(data);
            deferred.resolve(data);
        }
    });
    return deferred.promise();
}

function addBalance(user,amount) {
    var deferred = new $.Deferred();
    a = {"assetname":"transportcoin","assetquantity":String(amount),"sender":"7sigmapa","receiver":user,"program":"INTRAFFIC","userType":"program"};
    $.support.cors = true;
    console.log(a);
    $.ajax({
        url: "https://rg1.krypc.com:8080/WB/user/assetDistributionAPI",
        type: 'POST',
        data: JSON.stringify(a),
        dataType: 'json',
        contentType: 'application/json',
        async: true,
        success: function (data) {
            console.log(data);
            deferred.resolve(data);
        },
        error: function (data) {
            console.info(data);
            deferred.resolve(data);
        }
    });
    return deferred.promise();
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function formatDate(date) {
    return date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()
}

function formatDate2(date) {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes()
}

function formatDate3(date) {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + "-" + date.getDate();
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}